# SA_Practicas

# Practica 7

Configuración de CI

## Para la parte de los test

>    image: node:latest
>
>    stages:
>      - build
>      - test
>
>    cache:
>      paths:
>        - node_modules/
>
>    install_dependencies:
>      stage: build
>      script:
>        - npm install
>      artifacts:
>        paths:
>          - node_modules/
>
>    testing_testing:
>      stage: test
>      script: npm test


![Test Image 3](res3.png)
![Test Image 4](res4.png)
![Test Image 5](res5.png)

## Para la parte de SonarQube
Cree un archivo llamado sonar-project.properties dentro de la raíz de su repositorio.

> #SonarQube server
> #sonar.host.url & sonar.login are set by the Scanner CLI.
> #See https://docs.sonarqube.org/latest/analysis/gitlab-cicd/.

> #Project settings.
> sonar.projectKey=my-project
> sonar.projectName=My project
> sonar.projectDescription=My new interesting project.
> sonar.links.ci=https://gitlab.com/my-account/my-project/pipelines
> sonar.links.issue=https://jira.example.com/projects/MYPROJECT

> # Scan settings.
> sonar.projectBaseDir=.
> # Define the directories that should be scanned. Comma separated.
> sonar.sources=./src,./resources,./web
> sonar.test.inclusions=**/*Test.php
> sonar.php.coverage.reportPaths=./coverage/lcov.info
> sonar.php.file.suffixes=php
> sonar.sourceEncoding=UTF-8

> sonar.exclusions=,**/coverage/**

> #Fail CI pipeline if Sonar fails.
> sonar.qualitygate.wait=true

Agregue un escenario SonarQube a su archivogitlab-ci.yml. Se configura para que solo se ejecutara en la rama maestra de Git. 
Porque estoy usando SonarQube CommunityEdition, que solo admite el análisis de una rama por repositorio

> stages:
>   - analyze
> analyze:sonar:
>   stage: analyze
>   image:
>     name: sonarsource/sonar-scanner-cli:4.5
>     entrypoint: [""]
>   variables:
>     # Defines the location of the analysis task cache
>     SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
>     # Shallow cloning needs to be disabled.
>     # See https://docs.sonarqube.org/latest/analysis/gitlab-cicd/.
>     GIT_DEPTH: 0
>   cache:
>     key: "${CI_JOB_NAME}"
>     paths:
>       - .sonar/cache
>   script:
>     - sonar-scanner
>   rules:
>     # SonarQube CommunityEdition only supports analyzing a single branch.
>     # So only run on master.
>     - if: '$CI_COMMIT_BRANCH == "master"'
>       when: on_success
>     - when: never

Agregue las siguientes variables a través de la interfaz de usuario de GitLab CI. Tenga en cuenta que no debe enviar ninguna credencial a su repositor de Git

![Test Image 2](res2.png)

Primero que nada, necesitamos una ficha. Para obtener uno, inicie sesión en su instancia de Sonar y cree uno nuevo: ir a mi cuenta Haga clic en la pestaña Seguridad Ingrese un nombre de token y haga clic en Generar Copia el token generado

![Test Image 1](res1.png)

# Practica 6

## Instalaciones 
npm install — save express jest
npm install -D sonarqube-scanner jest-sonar-reporter supertest


## Se útilizo docker image

version:  '3'
services:
    sonarqube:
        container_name:  sonarqube
        image:  sonarqube:latest
        ports:
            -  "9000:9000"
            -  "9092:9092"

y se mando a ejecutar 
docker-compose -f docker-compose.sonar.yml up -d

## Para la configuración de sonarQube 

login: admin
password: admin 


Para mandar a correr las pruebas unitarias se utilizo esto 

npm run test

Y las automaticas se utilizo coverage

npm run sonar


## video

https://drive.google.com/file/d/1oDtTCqYqDyJeVZcahwKQjkhtfnDit1D2/view?usp=sharing

