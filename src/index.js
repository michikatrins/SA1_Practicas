const express =  require('express');
const app =  express();
const bodyParser = require('body-parser');
const cors = require('cors');
const bp = require('body-parser');
const request = require("request");
const port =  process.env.PORT  ||  3000

// Route to be tested
app.get('/solicitarPedidoRestaurante', (req, res) => {    
    return res.status(200).json({
        "id_pedido": 15,
        "id_restaurante": 10,
        "estado": "procesando",
        "total": 500.00,
        "descripcion": "10 hamburguesas"
       });
});


// Route to be tested
app.get('/verificarPedidoRestaurante', (req, res) => {
    return res.status(200).json({
            "id_pedido": 15,
            "id_restaurante": 10
        });
});


// Route to be tested
app.get('/verificarPedidoRepartidor2', (req, res) => {
    return res.status(200).json({
            "id_pedido": 15,
            "id_restaurante": 10
        });
});

// Application running on the door
let server = app.listen(port, () => {
    console.log(`Application running on ${port}`);
});
module.exports  = server;
