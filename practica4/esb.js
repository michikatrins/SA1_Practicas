const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const bp = require('body-parser');
const request = require("request");
const app = express()
const port = 9000;

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
});

//cliente  :D

app.post('/solicitarPedidoRestaurante',function(req,res){
    solicitarPedidoRestaurante(req,res);
    return;
});

app.post('/verificarPedidoRestaurante',function(req,res){
    verificarPedidoRestaurante(req,res);
    return;
});

app.post('/verificarPedidoRepartidor',function(req,res){
    verificarPedidoRepartidor(req,res);
    return;
});

//restaurante

app.post('/recibirPedidoDelCliente',function(req,res){
    recibirPedidoDelCliente(req,res);
    return;
});

app.post('/estadoPedidoCliente',function(req,res){
    estadoPedidoCliente(req,res);
    return;
});

app.post('/avisarPedidoRepartidor',function(req,res){
    avisarPedidoRepartidor(req,res);
    return;
});


//repartidor 

app.post('/recibirPedidoRestaurante',function(req,res){
    recibirPedidoRestaurante(req,res);
    return;
});

app.post('/estadoPedidoCliente',function(req,res){
    estadoPedidoCliente(req,res);
    return;
});

app.post('/verificarPedidoRepartidor2',function(req,res){
    verificarPedidoRepartidor2(req,res);
    return;
});
/*

REPARTIDOR

*/

function recibirPedidoRestaurante(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8083/recibirPedidoRestaurante",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function estadoPedidoCliente(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8083/estadoPedidoCliente",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function verificarPedidoRepartidor2(req, res2){
    var clientServerOptions = {
        uri: "http://localhost:8083/verificarPedidoRepartidor2",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

/*

RESTAURANTE

*/

function recibirPedidoDelCliente(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8082/recibirPedidoDelCliente",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function estadoPedidoCliente(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8082/estadoPedidoCliente",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function avisarPedidoRepartidor(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8082/avisarPedidoRepartidor",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}


/*

CLIENTE

*/

function verificarPedidoRepartidor(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8081/verificarPedidoRepartidor",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function verificarPedidoRestaurante(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8081/verificarPedidoRestaurante",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}

function solicitarPedidoRestaurante(req,res2){
    var clientServerOptions = {
        uri: "http://localhost:8081/solicitarPedidoRestaurante",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        res2.send(response.body);
        return;
    });
}
