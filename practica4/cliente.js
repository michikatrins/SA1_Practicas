const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');
const bp = require('body-parser');
const request = require("request");
const app = express()
const port = 5000;

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
});


//cliente 
function cliente1(){
  var clientServerOptions = {
      uri: "http://localhost:9000/solicitarPedidoRestaurante",
      body: JSON.stringify({
        "id_pedido": 15,
        "id_restaurante": 10,
        "estado": "procesando",
        "total": 500.00,
        "descripcion": "10 hamburguesas"
       }),
      method: 'post',
      headers: {
          'Content-Type': 'application/json'
      }
  }
  request(clientServerOptions, function (error, response) {
      //En caso encuentre error lo reportara
      if(error){
          return console.dir(error);
      }
      
      console.log("resultado 1");
      console.log(response.body);
      return;
  });
}

function cliente2(){
    var clientServerOptions = {
        uri: "http://localhost:9000/verificarPedidoRestaurante",
        body: JSON.stringify({
            "id_pedido": 15,
            "id_restaurante": 10
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 2");
        console.log(response.body);
        return;
    });
}

function cliente3(){
    var clientServerOptions = {
        uri: "http://localhost:9000/verificarPedidoRepartidor2",
        body: JSON.stringify({
            "id_pedido": 15,
            "id_restaurante": 10
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 3");
        console.log(response.body);
        return;
    });
}


/*
Aqui va lo de restaurante
*/

function cliente4(){
    var clientServerOptions = {
        uri: "http://localhost:9000/recibirPedidoDelCliente",
        body: JSON.stringify({
            "id_pedido": 180,
            "id_cliente": 4
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 4");
        console.log(response.body);
        return ;
    });
}


function cliente5(){
    var clientServerOptions = {
        uri: "http://localhost:9000/estadoPedidoCliente",
        body: JSON.stringify({
            "id_pedido":10
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 5");
        console.log(response.body);
        return ;
    });
}

function cliente6(){
    var clientServerOptions = {
        uri: "http://localhost:9000/avisarPedidoRepartidor",
        body: JSON.stringify({
            "id_repartidor":10,
            "id_pedido": 100
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 6");
        console.log(response.body);
        return ;
    });
}

/*
repartidor

*/

function cliente7(){
    var clientServerOptions = {
        uri: "http://localhost:9000/verificarPedidoRepartidor",
        body: JSON.stringify({
            "id_pedido": 1,
            "id_repartidor": 1,
            "entregado": true
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 7");
        console.log(response.body);
        return ;
    });
}

function cliente8(){
    var clientServerOptions = {
        uri: "http://localhost:9000/estadoPedidoCliente",
        body: JSON.stringify({
            "id_pedido": 15,
            "id_cliente": 10
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 8");
        console.log(response.body);
        return ;
    });
}

function cliente9(){
    var clientServerOptions = {
        uri: "http://localhost:9000/estadoPedidoCliente",
        body: JSON.stringify({
            "id_pedido": 15,
            "id_restaurante": 10
        }),
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function (error, response) {
        //En caso encuentre error lo reportara
        if(error){
            return console.dir(error);
        }
        console.log("resultado 9");
        console.log(response.body);
        return ;
    });
}


//llamada a los clientes

cliente2();
cliente1();
cliente3();
cliente4();
cliente5();
cliente6();
cliente7();
cliente8();
cliente9();