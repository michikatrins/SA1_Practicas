const express = require('express')
const bp = require('body-parser')
const app = express()
const port = 8082;

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

var fs = require("fs");

const user = {
    id_cliente : 150,
    estado : "en espera"
};

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})

//Recibir pedido del cliente

//Informar estado del pedido al cliente
//id_pedido
//id_cliente
//estado

/*
{
    "id_pedido": 180,
    "id_cliente": 4
}
*/

app.post('/recibirPedidoDelCliente', function (req, res) {
    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
        res.send({ respuesta: 'recibido' });
    });
    
    var respu =  Date() + " " +req.body+ " "+ "recibido\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' recibido\n');
    });

});

//Informar estado del pedido al cliente
//id_pedido
//id_cliente
//estado

/*
{
    "id_pedido":10
}
*/

app.post('/estadoPedidoCliente', function (req, res) {
    fs.readFile( __dirname + "/" + "pedido.json", 'utf8', function (err, data) {
       //res.end( JSON.stringify(user));
       res.send({ respuesta: 'en espera'});
    });

    
    var respu =  Date() + " " +req.body+ " "+ "en espera\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' en espera\n');
    });
});


//Avisar al repartidor que ya está listo el pedido
//id_repartidor
//estado
//id_pedido

/*
{
    "id_repartidor":10,
    "id_pedido": 100
}
*/

app.post('/avisarPedidoRepartidor', function (req, res) {
    fs.readFile( __dirname + "/" + "repartidor.json", 'utf8', function (err, data) {
        res.send({ respuesta: 'listo'});
     });

    var respu =  Date() + " " +req.body+ " "+"listo\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' listo\n');
    });
});
