const  gulp = require('gulp');
const zip = require('gulp-zip');
gulp.task('task', () => {
    return gulp.src(['./**', 'node_modules/*'])
      .pipe(zip('practica.zip'))
      .pipe(gulp.dest('zips'));
});
gulp.task('default', gulp.series('task'));