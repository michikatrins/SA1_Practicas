const express = require('express')
const bp = require('body-parser')
const app = express()
const port = 8083;

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

var fs = require("fs");

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})

//Recibir pedido del restaurante

/*
{
      "id_pedido": 15,
      "id_restaurante": 10
}
*/

app.post('/recibirPedidoRestaurante', function (req, res) {
    // First read existing users.
    fs.readFile( __dirname + "/" + "pedido.json", 'utf8', function (err, data) {
        res.send({ respuesta: 'agregado' });
    });

    var respu =  Date() + " " +req.body+ " "+ "activo\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' activo\n');
    });
 });

//Informar estado del pedido al cliente
//id_pedido
//id_cliente
//estado

/*
{
      "id_pedido": 15,
      "id_cliente": 10
}
*/

app.post('/estadoPedidoCliente', function (req, res) {
    fs.readFile( __dirname + "/" + "pedido.json", 'utf8', function (err, data) {
        res.send({ respuesta: 'activo' });
    });

    var respu =  Date() + " " +req.body+ " "+ "activo\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' activo\n');
    });
});

//Marcar como entregado
//id_pedido
//id_repartidor
//estado

/*
{
      "id_pedido": 1,
      "id_repartidor": 1,
      "entregado": true
}
*/

app.post('/verificarPedidoRepartidor2', function (req, res) {
	console.log(req.body);
    fs.readFile( __dirname + "/" + "repartidor.json", 'utf8', function (err, data) {
        res.send({ respuesta: 'entregado' });
     });

     
    var respu =  Date() + " " +req.body+ " "+ "entregado\n";
    fs.appendFile('../log.txt', respu , function (err) {
    if (err) return console.log(err);
        console.log(Date()+' entregado\n');
    });
});
