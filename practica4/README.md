# SA1_Practicas

Katherine Serrano
201314697

## PRÁCTICA 5

1. Tomar como base la práctica # 4.
2. Asegurar que se cumpla con un versionamiento SEMVER

3. Crear las ramas básicas master / developer
    >Estas ya estaban creadas desde el inicio del proyecto

4. Incorporar herramienta para construcción de artefacto(s):

a. Si es código interpretado por medio de compresión de archivos.
b. Si es código compilado por medio de compilación propia.

Se útilizo la herramienta gulp 
utilizando el archivo gulpfile.js donde se utiliza la herramienta de construcción. 


5. Enviar repositorio con las nuevas ramas, tags, herramienta de
construcción y pruebas de la construcción

Se crearon las nuevas ramas 
> Master
> Developer
![Test Image 1](prueba2.png)

El tag a crear es el tag v1.0.0 entrega final de esta ciclo 
![Test Image 1](prueba3.png)

Se puede observar en la carpeta zips como se agrega el zip de la práctica
![Test Image 1](prueba.png)