const request =  require('supertest')
const server =  require('../src/index')
describe('solicitarPedidoRestaurante', () => {
    it('solicitarPedidoRestaurante', async (done) => {
        const res =  await  request(server)
        .get('/solicitarPedidoRestaurante')
        .send({
        "id_pedido": 15,
        "id_restaurante": 10,
        "estado": "procesando",
        "total": 500.00,
        "descripcion": "10 hamburguesas"
       });
        expect(res.statusCode).toEqual(200);
        //expect(res.body).toHaveProperty('nome');
        done();
    })
})

describe('verificarPedidoRestaurante', () => {
    it('verificarPedidoRestaurante', async (done) => {
        const res =  await  request(server)
        .get('/verificarPedidoRestaurante')
        .send({
            "id_pedido": 15,
            "id_restaurante": 10
        });
        expect(res.statusCode).toEqual(200);
        //expect(res.body).toHaveProperty('nome');
        done();
    })
})

describe('verificarPedidoRepartidor2', () => {
    it('verificarPedidoRepartidor2', async (done) => {
        const res =  await  request(server)
        .get('/verificarPedidoRepartidor2')
        .send({
            "id_pedido": 15,
            "id_restaurante": 10
        });
        expect(res.statusCode).toEqual(200);
        //expect(res.body).toHaveProperty('nome');
        done();
    })
})

afterAll(async  done  => {
    // close server conection
    server.close();
    done();
});
